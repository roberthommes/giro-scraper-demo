import os
import shutil
import json

from selenium import webdriver
from selenium.webdriver.chrome.options import Options

from my_lib.my_script import get_string

BIN_DIR = "/tmp/bin"
CURR_BIN_DIR = os.getcwd() + "/bin"


def _init_bin(executable_name):
    if not os.path.exists(BIN_DIR):
        # logger.info("Creating bin folder")
        os.makedirs(BIN_DIR)

    # logger.info("Copying binaries for " + executable_name + " in /tmp/bin")

    currfile = os.path.join(CURR_BIN_DIR, executable_name)
    newfile = os.path.join(BIN_DIR, executable_name)

    shutil.copy2(currfile, newfile)

    # logger.info("Giving new binaries permissions for lambda")

    os.chmod(newfile, 0o775)


def lambda_handler(event, context):

    _init_bin("headless-chromium")
    _init_bin("chromedriver")

    options = Options()

    options.add_argument("--headless")
    options.add_argument("--disable-gpu")
    options.add_argument("--no-sandbox")
    options.add_argument('--disable-dev-shm-usage')
    options.add_argument('--disable-gpu-sandbox')
    options.add_argument("--single-process")
    options.add_argument('window-size=1920x1080')
    options.add_argument(
        '"user-agent=Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.100 Safari/537.36"' # noqa
    )

    options.binary_location = "/tmp/bin/headless-chromium"
    driver = webdriver.Chrome("/tmp/bin/chromedriver", options=options)

    driver.get('https://www.google.com/')
    body = f"Headless Chrome Initialized, Page title: {driver.title} My String: {get_string()}"

    driver.close()
    driver.quit()

    return {
        "statusCode": 200,
        "body": json.dumps(
            {
                "response": body,
            }
        ),
    }
